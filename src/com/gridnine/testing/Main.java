package com.gridnine.testing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        /**
         * Создаем перелеты при помощи фабрики.
         */
        List<Flight> flight = FlightBuilder.createFlights();

        /**
         * Выводим на консоль то, что отфильтровано.
         */
        FilterResultPrinter.printResult(flight);

    }

}
