package com.gridnine.testing;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FilterFlightUtilImpl implements FilterFlightUtil{

    @Override
    public List<Flight> departureBeforeNow(List<Flight> flightList) {
        List<Flight> sortedFlight = new ArrayList<>();
        for (int index = 0; index < flightList.size(); index++) {
            Flight flight = flightList.get(index);
            List<Segment> segments = flight.getSegments();
            for (int i = 0; i < segments.size(); i++) {
                Segment segment = segments.get(i);
                if ((LocalDateTime.now()).isBefore(segment.getDepartureDate())){
                    sortedFlight.add(flight);
                    }
                }
            }
        return sortedFlight;
    }

    @Override
    public List<Flight> arrivalBeforeDeparture(List<Flight> flightList) {
        List<Flight> sortedFlight = new ArrayList<>();
        for (int index = 0; index < flightList.size(); index++) {
            Flight flight = flightList.get(index);
            List<Segment> segments = flight.getSegments();
            for (int i = 0; i < segments.size(); i++) {
                Segment segment = segments.get(i);
                if (segment.getArrivalDate().isBefore(segment.getDepartureDate())){
                    sortedFlight.add(flight);
                }
            }
        }
        return sortedFlight;
    }

    @Override
    public List<Flight> arrivalPerTwoHoursDeparture(List<Flight> flightList) {
        List<Flight> sortedFlight = new ArrayList<>();
        for (int index = 0; index < flightList.size(); index++) {
            Flight flight = flightList.get(index);
            List<Segment> segments = flight.getSegments();
            for (int i = 0; i < segments.size(); i++) {
                Segment segment = segments.get(i);
                if (segment.getArrivalDate().isBefore(segment.getDepartureDate().plusHours(2))){
                    sortedFlight.add(flight);
                }
            }
        }
        return sortedFlight;
    }
}
