package com.gridnine.testing;

import com.gridnine.testing.Flight;

import java.util.Date;
import java.util.List;

public interface FilterFlightUtil {
    /**
     * Получение коллекции полетов у которых вылет произошел до текущего момента,
     * @param flightList коллекция неотфильтрованных полетов.
     * @return отфильтрованная коллекцию Flight.
     */
    public List<Flight> departureBeforeNow(List<Flight> flightList) ;

    /**
     * Получение коллекции полетов с датой прилета раньше даты вылета.
     * @param flightList коллекция неотфильтрованных полетов.
     * @return отфильрованная коллекция Flight.
     */
    public List<Flight> arrivalBeforeDeparture(List<Flight> flightList);

    /**
     * Получение коллекции, где общее время проведенное на земле превышает 2 часа.
     * @param flightList коллекция неотфильтрованных полетов.
     * @return отфильтрованная коллекция Flight.
     */
    public List<Flight> arrivalPerTwoHoursDeparture(List<Flight> flightList);

}
