package com.gridnine.testing;

import java.util.List;
/**
 *Создаем принтер для вывода отсортированных значений из спаска полетов.
 */

public class FilterResultPrinter {
    public static void  printResult(List<Flight> flightList) {

        FilterFlightUtilImpl filterOne = new FilterFlightUtilImpl();
        List<Flight> departureBeforeNow = filterOne.departureBeforeNow(flightList);

        FilterFlightUtilImpl filterTwo = new FilterFlightUtilImpl();
        List<Flight> arrivalBeforeDeparture = filterTwo.arrivalBeforeDeparture(flightList);

        FilterFlightUtilImpl filterThree = new FilterFlightUtilImpl();
        List<Flight> arrivalPerTwoHoursDeparture = filterThree.arrivalPerTwoHoursDeparture(flightList);

        System.out.println("Вылет до текущего момента времени:");
        System.out.println(departureBeforeNow);
        System.out.println("Дата прилета раньше даты вылета:");
        System.out.println(arrivalBeforeDeparture);
        System.out.println("Общее время на земле превышает 2 часа:");
        System.out.println(arrivalPerTwoHoursDeparture);
    }
}
